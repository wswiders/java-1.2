package com.capgemini.hanoi.system;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.hanoi.Ring;
import com.capgemini.hanoi.Tower;
import com.capgemini.hanoi.exception.IllegalMoveException;
import com.capgemini.hanoi.exception.IllegalTowerNumberException;
import com.capgemini.hanoi.exception.WrongNumberOfRingsException;

public class HanoiSystemImp implements HanoiSystem {

	private List<Tower> towers = new ArrayList<>();
	private int numberOfRings;

	@Override
	public String printBoard() {
		StringBuilder builder = new StringBuilder("Wieza hanoi\n---------\n");
		for (Tower tower : towers) {
			builder.append(tower.printTower()).append("\n");
		}
		return builder.toString();
	}

	@Override
	public boolean isFinished() {
		for (int i = 1; i < towers.size(); i++) {
			if (towers.get(i).getRings().size() == this.numberOfRings) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void initializeGame(int numberOfRings) throws WrongNumberOfRingsException {
		if (numberOfRings <= 1 || numberOfRings >= 100) {
			throw new WrongNumberOfRingsException();
		}
		this.numberOfRings = numberOfRings;
		towers.add(new Tower(0, this.numberOfRings));
		towers.add(new Tower(1));
		towers.add(new Tower(2));
	}

	@Override
	public void moveRing(int sourceTower, int destinationTower)
			throws IllegalMoveException, IllegalTowerNumberException {
		if (sourceTower >= 0 && sourceTower <= 2 && destinationTower >= 0 && destinationTower <= 2) {
			Ring ringToMove = towers.get(sourceTower).getLastRing();
			if (isMoveValid(sourceTower, destinationTower) && ringToMove != null) {
				towers.get(destinationTower).addRing(ringToMove);
				towers.get(sourceTower).removeLastElement();
			} else {
				throw new IllegalMoveException();
			}
		} else {
			throw new IllegalTowerNumberException();
		}

	}

	private boolean isMoveValid(int sourceTower, int destinationTower) throws IllegalMoveException {
		if (towers.get(sourceTower).getLastRing() == null) {
			return false;
		}
		if (towers.get(destinationTower).getLastRing() == null) {
			return true;
		}
		int sourceTowerLastRingSize = towers.get(sourceTower).getLastRingSize();
		int destinationTowerLastRingSize = towers.get(destinationTower).getLastRingSize();
		if (destinationTowerLastRingSize != 0) {
			return sourceTowerLastRingSize < destinationTowerLastRingSize;
		} else {
			return true;
		}
	}

}
