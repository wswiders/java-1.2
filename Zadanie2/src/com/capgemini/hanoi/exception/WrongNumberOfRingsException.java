package com.capgemini.hanoi.exception;

public class WrongNumberOfRingsException extends Exception {

	public WrongNumberOfRingsException() {
		super("Podano nieprawidlowa liczbe.");
	}
}
