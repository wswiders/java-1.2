package com.capgemini.hanoi.exception;

public class IllegalTowerNumberException extends Exception {
	
	public IllegalTowerNumberException() {
		super("Wprowadzono nieprawidlowy numer wiezy");
	}
}
