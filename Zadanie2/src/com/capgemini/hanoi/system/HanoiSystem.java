package com.capgemini.hanoi.system;

import com.capgemini.hanoi.exception.IllegalMoveException;
import com.capgemini.hanoi.exception.IllegalTowerNumberException;
import com.capgemini.hanoi.exception.WrongNumberOfRingsException;

public interface HanoiSystem {

	public boolean isFinished();
	
	public void initializeGame(int numberOfRings) throws WrongNumberOfRingsException;
	
	public void moveRing(int sourceTower, int destinationTower) throws IllegalMoveException, IllegalTowerNumberException;

	public String printBoard();
}
