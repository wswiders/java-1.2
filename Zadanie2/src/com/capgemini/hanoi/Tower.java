package com.capgemini.hanoi;

import java.util.ArrayList;
import java.util.List;

public class Tower {
	
	private List<Ring> rings = new ArrayList<>();
	private int id;
	StringBuilder builder;
	
	public Tower(int id) {
		this.id=id;
	}

	public Tower(int id, int numberOfRings){
		this.id=id;
		setRingsArray(numberOfRings);
	}
	
	private void setRingsArray(int numberOfRings){
		int size = numberOfRings;
		for (int i = 0; i < numberOfRings; i++) {
			rings.add(new Ring(size--));
		}
	}

	public List<Ring> getRings() {
		return this.rings;
	}
	
	public Ring getLastRing(){
		if(rings.isEmpty()){
			return null;
		}
		return rings.get(rings.size()-1);
	}
	
	public int getLastRingSize(){
		if(rings.isEmpty()){
			return 0;
		}
		return rings.get(rings.size()-1).getSize();
	}
	
	public String printTower(){
		builder = new StringBuilder().append(id+"- ");
		if(rings.size()>=1){
			for (Ring ring : rings) {
				builder.append(ring.getSize()).append(" ");
			}
		}
		return builder.toString();
	}
	
	public void removeLastElement(){
		rings.remove(rings.size()-1);
	}
	
	public void addRing(Ring ring){
		rings.add(ring);
	}
}
