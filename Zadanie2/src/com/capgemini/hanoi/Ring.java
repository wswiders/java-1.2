package com.capgemini.hanoi;

public class Ring {

	private int size;

	Ring(int size) {
		this.size = size;
	}

	protected int getSize() {
		return size;
	}
}
