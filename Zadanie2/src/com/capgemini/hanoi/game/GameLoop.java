package com.capgemini.hanoi.game;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.capgemini.hanoi.exception.IllegalMoveException;
import com.capgemini.hanoi.exception.IllegalTowerNumberException;
import com.capgemini.hanoi.exception.WrongNumberOfRingsException;
import com.capgemini.hanoi.system.HanoiSystemImp;

public class GameLoop {

	public static void main(String[] args) {

		HanoiSystemImp hanoiGame = new HanoiSystemImp();
		Scanner scanner = new Scanner(System.in);

		System.out.println(
				"Gra Hanoi Tower.\nAby przesuwac krazki miedzy wiezami nalezy podac numery wiez [0, 1 lub 2]\n");

		while (true) {
			int numberOfRings;
			try {
				System.out.print("Inicjalizacja gry. Podaj calkowita liczbe krazkow z przedzialu<1,100>: ");
				numberOfRings = scanner.nextInt();
				hanoiGame.initializeGame(numberOfRings);
				System.out.println(hanoiGame.printBoard());
				break;
			} catch (WrongNumberOfRingsException e) {
				System.out.println("Blad. Podano nieprawidlowa liczbe.");
			} catch (InputMismatchException e) {
				System.out.println("Blad. Nalezy podac liczbe.");
				scanner.next();
			}
		}

		while (!hanoiGame.isFinished()) {
			int sourceTower;
			int destinationTower;
			try {
				System.out.print("Podaj wieze z krazkiem: ");
				sourceTower = scanner.nextInt();
				System.out.print("Podaj wieze docelowa: ");
				destinationTower = scanner.nextInt();
				hanoiGame.moveRing(sourceTower, destinationTower);
				System.out.println(hanoiGame.printBoard());
			} catch (IllegalMoveException e) {
				System.out.println("Blad. Nieprawidlowy ruch.");
			} catch (IllegalTowerNumberException e) {
				System.out.println("Blad. Podano nieprawidlowy numer wiezy.");
			} catch (InputMismatchException e) {
				System.out.println("Blad. Nalezy podac liczbe.");
				scanner.next();
			}
		}
		scanner.close();
		System.out.println("Gratulacje! Zagadka rozwiazana. Koniec Gry.");
	}
}
