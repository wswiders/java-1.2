package com.capgemini.hanoi.system;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.hanoi.exception.IllegalMoveException;
import com.capgemini.hanoi.exception.IllegalTowerNumberException;
import com.capgemini.hanoi.exception.WrongNumberOfRingsException;

public class HanoiSystemImpTest {

	HanoiSystemImp hanoiGame;
	
	@Before
	public void setUp() throws Exception {
		hanoiGame = new HanoiSystemImp();
	}

	@Test(expected=WrongNumberOfRingsException.class)
	public void shouldThrowWrongNumberExceptionWhenNumerIsLessThan1() throws Exception{
		hanoiGame.initializeGame(0);
	}
	
	@Test(expected=WrongNumberOfRingsException.class)
	public void shouldThrowWrongNumberExceptionWhenNumerIsOver100() throws Exception{
		hanoiGame.initializeGame(101);
	}
	
	@Test(expected=IllegalTowerNumberException.class)
	public void shouldThrowIllegalTowerNumberExceptionWhenFirstTowerNumberLess0() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(-1, 1);
	}
	
	@Test(expected=IllegalTowerNumberException.class)
	public void shouldThrowIllegalTowerNumberExceptionWhenSecondTowerNumberLess0() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, -1);
	}
	
	@Test(expected=IllegalTowerNumberException.class)
	public void shouldThrowIllegalTowerNumberExceptionWhenFirstTowerNumberOver2() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(3, 1);
	}
	
	@Test(expected=IllegalTowerNumberException.class)
	public void shouldThrowIllegalTowerNumberExceptionWhenSecondTowerNumberOver2() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, 3);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void shouldThrowIllegalMoveExceptionWhenMoveFromEmptyTowerToEmptyTower() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, 2);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void shouldThrowIllegalMoveExceptionWhenMoveFromEmptyTowerToFillTower() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, 0);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void shouldThrowIllegalMoveExceptionWhenMoveRingToSameEmptyTower() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, 1);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void shouldThrowIllegalMoveExceptionWhenMoveRingToSameFillTower() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(1, 1);
	}
	
	@Test(expected=IllegalMoveException.class)
	public void shouldThrowIllegalMoveExceptionWhenTryToSetBigerRingOnSmaller() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(0, 1);
		hanoiGame.moveRing(0, 1);
	}
	
	@Test
	public void shouldNotEndGameWhenAllRingsAreNotOnOtherTower() throws Exception{
		hanoiGame.initializeGame(3);
		hanoiGame.moveRing(0, 1);
		hanoiGame.moveRing(0, 2);
		hanoiGame.moveRing(1, 2);
		hanoiGame.moveRing(0, 1);
		assertFalse(hanoiGame.isFinished());
	}
	
	@Test
	public void shouldEndGameWhenAllRingsAreOnOtherTower() throws Exception{
		hanoiGame.initializeGame(2);
		hanoiGame.moveRing(0, 1);
		hanoiGame.moveRing(0, 2);
		hanoiGame.moveRing(1, 2);
		assertTrue(hanoiGame.isFinished());
	}
}
