package com.capgemini.hanoi.exception;

public class IllegalMoveException extends Exception {

	public IllegalMoveException() {
		super("Wykonano nieprawidlowy ruch");
	}
}
